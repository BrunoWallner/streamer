use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use ringbuf::SharedRb;
use ringbuf::{Consumer, Producer};
use std::mem::MaybeUninit;
use std::sync::Arc;

const LATENCY: f32 = 100.0;

pub trait Anyway {
    fn anyway(&self) -> Result<(), ()>;
}
impl<T, E> Anyway for Result<T, E> {
    fn anyway(&self) -> Result<(), ()> {
        match self {
            Ok(_) => Ok(()),
            Err(_) => Err(()),
        }
    }
}

pub struct OutputController {
    producer: Producer<f32, Arc<SharedRb<f32, Vec<MaybeUninit<f32>>>>>,
}
impl OutputController {
    pub fn push_data(&mut self, data: Vec<f32>) {
        for data in data {
            let _ = self.producer.push(data);
        }
    }
}

pub struct Output {
    host: cpal::platform::Host,
    // stream must stay in scope
    stream: Option<cpal::Stream>,
}
impl Output {
    pub fn new() -> Self {
        let host = cpal::default_host();

        return Self { host, stream: None };
    }
    /// returns: `channel_count`, `sampling_rate` and `CaptureReceiver`
    pub fn init(&mut self, channel_count: u16, sampling_rate: u32) -> OutputController {
        let latency_frames = (LATENCY / 1_000.0) * sampling_rate as f32;
        let latency_samples = latency_frames as usize * channel_count as usize;
        let ring = SharedRb::new(latency_samples * 2);
        let (mut producer, consumer) = ring.split();

        for _ in 0..latency_samples {
            // cannot fail
            producer.push(0.0).unwrap();
        }

        let output_controller = OutputController { producer };

        let stream =
            stream_audio_to_distributor(&self.host, consumer, channel_count, sampling_rate);
        self.stream = Some(stream);

        output_controller
    }
}

fn stream_audio_to_distributor(
    host: &cpal::platform::Host,
    mut consumer: Consumer<f32, Arc<SharedRb<f32, Vec<MaybeUninit<f32>>>>>,
    channel_count: u16,
    sampling_rate: u32,
    // returns channel-count, stream and sampling-rate
) -> cpal::Stream {
    let device = host.default_output_device().unwrap();
    let config = cpal::StreamConfig {
        channels: channel_count,
        sample_rate: cpal::SampleRate(sampling_rate),
        buffer_size: cpal::BufferSize::Default,
    };

    #[allow(unused_must_use)]
    let stream = device.build_output_stream(
        &config.into(),
        move |data: &mut [f32], _: &_| {
            for sample in data {
                if let Some(d) = consumer.pop() {
                    *sample = d;
                } else {
                    *sample = 0.0;
                }
            }
        },
        |e| eprintln!("error occurred on capture-stream: {}", e),
    );
    let stream = stream.unwrap();

    stream.play().unwrap();

    stream
}
