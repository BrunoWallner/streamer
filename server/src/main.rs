mod audio;

use audio::Output;
use audio::OutputController;
use common::*;
use std::net::UdpSocket;

fn main() {
    loop {
        setup_logger().unwrap();
        let mut socket = UdpSocket::bind("0.0.0.0:3333").unwrap();
        let event = get_event_and_connect(&mut socket);
        match event {
            Ok(Event::Ping) => {
                // send pingpack
                if send_event(&mut socket, &Event::PingBack).is_err() {
                    warn!("failed to send event to client");
                    continue;
                }
            }
            _ => {
                warn!("invalid event from client");
                continue;
            }
        }

        let mut output_controller: Option<OutputController> = None;
        // must not get dropped
        // alternatively use `mem::forget` but I like this method more
        let mut _output: Option<Output> = None;

        loop {
            let event = get_event(&mut socket);

            match event {
                Ok(Event::Ping) => {
                    if send_event(&mut socket, &Event::PingBack).is_err() {
                        warn!("failed to send event to client");
                        break;
                    }
                }
                Ok(Event::AudioConfig {
                    sampling_rate,
                    channel_count,
                }) => {
                    let mut o = Output::new();
                    let o_c = o.init(channel_count, sampling_rate);
                    _output = Some(o);
                    output_controller = Some(o_c);
                    if send_event(&mut socket, &Event::AudioReady).is_err() {
                        warn!("failed to send event to client");
                        break;
                    }
                }
                Ok(Event::Data(ref data)) => {
                    let real_data = Quality::bits_to_data(&data);
                    if let Some(output_controller) = &mut output_controller {
                        output_controller.push_data(real_data);
                    } else {
                        warn!("audio backend not yet initialized");
                    }
                }
                _ => warn!("invalid event from client"),
            }
        }
    }
}
