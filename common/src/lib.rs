pub mod quality;
pub use quality::Quality;

use flate2::read::ZlibDecoder;
use flate2::write::ZlibEncoder;
use flate2::Compression;
use serde::{Deserialize, Serialize};
use std::io::{Read, Write};

pub use log::*;
pub use std::io::Result as IoResult;
pub use std::net::UdpSocket;

const BUFFER_SIZE: usize = 1024 * 32;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Event {
    AudioConfig {
        channel_count: u16,
        sampling_rate: u32,
    },
    Data(Vec<Vec<bool>>),
    AudioReady,
    Ping,
    PingBack,
}

pub trait Anyway<T> {
    fn anyway(self) -> Result<T, ()>;
}
impl<T, E> Anyway<T> for Result<T, E> {
    fn anyway(self) -> Result<T, ()> {
        match self {
            Ok(t) => Ok(t),
            Err(_) => Err(()),
        }
    }
}
pub fn send_event(socket: &mut UdpSocket, event: &Event) -> Result<(), ()> {
    let encoded = bincode::serialize(event).anyway()?;
    let mut compressor = ZlibEncoder::new(Vec::new(), Compression::default());
    compressor.write_all(&encoded).anyway()?;
    let compressed = compressor.finish().anyway()?;

    socket.send(&compressed).anyway()?;

    Ok(())
}

pub fn get_event(socket: &mut UdpSocket) -> Result<Event, ()> {
    // heap allocate to not overflow stack
    let mut buf: Box<[u8; BUFFER_SIZE]> = Box::new([0x00; BUFFER_SIZE]);
    let amount = socket.recv(&mut (*buf)).anyway()?;

    // println!("{}", &buf[0..amount]);

    let mut decompressor = ZlibDecoder::new(&buf[0..amount]);
    let mut buffer: Vec<u8> = Vec::new();
    decompressor.read_to_end(&mut buffer).anyway()?;

    let event: Event = bincode::deserialize(&buffer).anyway()?;
    Ok(event)
}

pub fn get_event_and_connect(socket: &mut UdpSocket) -> Result<Event, ()> {
    // heap allocate to not overflow stack
    let mut buf: Box<[u8; BUFFER_SIZE]> = Box::new([0x00; BUFFER_SIZE]);
    let (amount, src) = socket.recv_from(&mut (*buf)).anyway()?;
    socket.connect(src).unwrap();

    let mut decompressor = ZlibDecoder::new(&buf[0..amount]);
    let mut buffer: Vec<u8> = Vec::new();
    decompressor.read_to_end(&mut buffer).anyway()?;

    let event: Event = bincode::deserialize(&buffer).anyway()?;
    Ok(event)
}

pub fn setup_logger() -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}] {}",
                record.target(),
                record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}
