type Representation = u32;

#[derive(Copy, Clone, Debug)]
pub enum Quality {
    Low,
    Mid,
    High,
    Custom(u32),
}

impl Quality {
    pub fn data_to_bits(&self, data: &[f32], volume: f32) -> Vec<Vec<bool>> {
        // MUST not be greater than 32,
        let mut bits_amount: u32 = match self {
            Quality::Low => 10,
            Quality::Mid => 16,
            Quality::High => 32,
            Quality::Custom(custom) => *custom,
        };
        if bits_amount > Representation::BITS {
            bits_amount = Representation::BITS
        }
        data.iter()
            .map(|d| {
                // assert!(d <= &0.5 && d >= &-0.5);
                // if d > &0.5 || d < &-0.5 {
                //     eprintln!("invalid range: {}", d);
                // }
                let d = d * volume;
                let d = d.clamp(-0.5, 0.5);
                let d = ((d + 0.5) * Representation::MAX as f32) as Representation;
                let mut bits: Vec<bool> = Vec::with_capacity(bits_amount as usize);
                let start = Representation::BITS - bits_amount;
                let end = Representation::BITS;
                for n in start..end {
                    bits.push((d >> n & 1) == 1);
                }
                bits
            })
            .collect()
    }

    pub fn bits_to_data(bits: &[Vec<bool>]) -> Vec<f32> {
        let mut real_data: Vec<f32> = Vec::new();
        for bits in bits {
            let mut value: f32 = 0.0;
            for bit in bits {
                if *bit {
                    value += 0.5
                } else {
                    value -= 0.5
                }
                value /= 2.0;
            }
            real_data.push(value);
        }

        real_data
    }
    // pub fn bits_to_data(bits: &[Vec<bool>]) -> Vec<f32> {
    //     let mut real_data: Vec<f32> = Vec::new();
    //     for b in bits {
    //         let divisor: f32 = 2_f32.powi(b.len() as i32);
    //         let volume = Self::bits_to_representation(b.to_vec());
    //         let volume = volume as f32 / divisor - 0.5;
    //         real_data.push(volume);
    //     }
    //     real_data
    // }

    // pub fn bits_to_representation(mut bits: Vec<bool>) -> Representation {
    //     bits.reverse();
    //     bits.iter()
    //         .fold(0, |result, &bit| (result << 1) ^ bit as Representation)
    // }
}
