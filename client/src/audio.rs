use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use ringbuf::SharedRb;
use ringbuf::{Consumer, Producer};
use std::mem::MaybeUninit;
use std::sync::Arc;

const LATENCY: f32 = 200.0;

pub trait Anyway {
    fn anyway(&self) -> Result<(), ()>;
}
impl<T, E> Anyway for Result<T, E> {
    fn anyway(&self) -> Result<(), ()> {
        match self {
            Ok(_) => Ok(()),
            Err(_) => Err(()),
        }
    }
}

pub struct InputController {
    consumer: Consumer<f32, Arc<SharedRb<f32, Vec<MaybeUninit<f32>>>>>,
}
impl InputController {
    pub fn pull_data(&mut self) -> Vec<f32> {
        let mut data = Vec::new();
        while let Some(d) = self.consumer.pop() {
            data.append(&mut vec![d]);
        }
        data
    }
}

pub struct Input {
    host: cpal::platform::Host,
    // stream must stay in scope
    stream: Option<cpal::Stream>,
}
impl Input {
    pub fn new() -> Self {
        let host = cpal::default_host();

        return Self { host, stream: None };
    }
    /// returns: `channel_count`, `sampling_rate` and `CaptureReceiver`
    pub fn init(&mut self) -> (u16, u32, InputController) {
        let latency_frames = (LATENCY / 1_000.0) * 48_000.0;
        let latency_samples = latency_frames as usize * 2;
        let ring = SharedRb::new(latency_samples * 2);
        let (mut producer, consumer) = ring.split();

        for _ in 0..latency_samples {
            // cannot fail
            producer.push(0.0).unwrap();
        }

        let (channels, sampling_rate, stream) = stream_audio_to_distributor(&self.host, producer);
        self.stream = Some(stream);

        let input_controller = InputController { consumer };

        (channels, sampling_rate, input_controller)
    }
}

fn stream_audio_to_distributor(
    host: &cpal::platform::Host,
    mut producer: Producer<f32, Arc<SharedRb<f32, Vec<MaybeUninit<f32>>>>>,
    // returns channel-count, stream and sampling-rate
) -> (u16, u32, cpal::Stream) {
    let device = host.default_output_device().unwrap();

    let default_config = device.default_input_config().unwrap();
    let channels = default_config.channels();
    let sample_rate = default_config.sample_rate();
    let config = cpal::StreamConfig {
        channels,
        sample_rate,
        buffer_size: cpal::BufferSize::Fixed(512),
    };

    #[allow(unused_must_use)]
    let stream = device.build_input_stream(
        &config.into(),
        move |data: &[f32], _: &_| {
            let _ = producer.push_slice(data);
        },
        |e| eprintln!("error occurred on capture-stream: {}", e),
    );
    let stream = stream.unwrap();

    stream.play().unwrap();

    (channels, sample_rate.0, stream)
}
