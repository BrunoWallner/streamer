mod audio;
use audio::Input;

use common::*;
use std::io::stdin;
use std::net::UdpSocket;
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::sleep;
use std::time::Duration;

const CONNECTION_TIMEOUT: Duration = Duration::from_secs(2);
const CONNECTION_IP: &str = "192.168.10.68:3333";
// const CONNECTION_IP: &str = "localhost:3333";

const SEND_RATE: Duration = Duration::from_millis(5);

fn main() {
    setup_logger().unwrap();

    let quality = Arc::new(Mutex::new(Quality::High));
    let volume = Arc::new(Mutex::new(1.0));
    input_settings(quality.clone(), volume.clone());

    let mut input = Input::new();
    let (channel_count, sampling_rate, mut input_controller) = input.init();

    let mut socket = connect();

    let info = Event::AudioConfig {
        channel_count,
        sampling_rate,
    };
    send_event(&mut socket, &info).unwrap();

    // wait for server
    loop {
        if let Ok(event) = get_event(&mut socket) {
            if event == Event::AudioReady {
                break;
            }
        }
    }

    // clear buffer
    let _ = input_controller.pull_data();
    loop {
        let data = input_controller.pull_data();
        let quality = quality.lock().unwrap();
        let volume = volume.lock().unwrap();
        let reduced_data = quality.data_to_bits(&data, *volume);
        if !reduced_data.is_empty() {
            let event = Event::Data(reduced_data);
            if send_event(&mut socket, &event).is_err() {
                warn!("lost connection to server");
                break;
            }
        }
        sleep(SEND_RATE);
    }
}

fn connect() -> UdpSocket {
    let mut socket = UdpSocket::bind("0.0.0.0:3334").unwrap();
    socket.connect(CONNECTION_IP).unwrap();
    socket.set_read_timeout(Some(CONNECTION_TIMEOUT)).unwrap();

    loop {
        send_event(&mut socket, &Event::Ping).unwrap();
        match get_event(&mut socket) {
            Ok(event) => match event {
                Event::PingBack => {
                    info!("connnected");
                    socket.set_read_timeout(None).unwrap();
                    return socket;
                }
                _ => {
                    warn!("{} unreachable", CONNECTION_IP);
                }
            },
            Err(_) => warn!("{} unreachable, trying again ...", CONNECTION_IP),
        }
        sleep(CONNECTION_TIMEOUT)
    }
}

fn input_settings(quality: Arc<Mutex<Quality>>, volume: Arc<Mutex<f32>>) {
    thread::spawn(move || loop {
        // get value and meta
        let mut string = String::new();
        let _ = stdin().read_line(&mut string);
        let string = string.trim().to_string();
        let split: Vec<&str> = string.split(' ').collect();
        let (meta, value) = if split.len() == 2 {
            (split[0], split[1])
        } else {
            ("", "")
        };

        let mut success: bool = false;
        match meta.to_ascii_lowercase().as_str() {
            "q" | "quality" => {
                if let Ok(val) = value.parse::<u32>() {
                    if let Ok(mut quality) = quality.lock() {
                        *quality = Quality::Custom(val);
                        info!("set Quality to: {} bits / sample", val);
                        success = true;
                    }
                }
            }
            "v" | "volume" => {
                if let Ok(val) = value.parse::<f32>() {
                    if let Ok(mut volume) = volume.lock() {
                        *volume = val / 100.0;
                        info!("set volume to: {}%", val);
                        success = true;
                    }
                }
            }
            _ => (),
        }
        if !success {
            warn!("invalid user input");
        }

        //     // remove % char
        //     string.pop();
        //     if let Ok(val) = string.parse::<f32>() {
        //         if let Ok(mut volume) = volume.lock() {
        //             *volume = val / 100.0;
        //             info!("set volume to: {}%", val);
        //             success = true;
        //         }
        //     }
        // } else {
        //     if let Ok(val) = string.parse::<u32>() {
        //         if let Ok(mut quality) = quality.lock() {
        //             *quality = Quality::Custom(val);
        //             info!("set Quality to: {} bits / sample", val);
        //             success = true;
        //         }
        //     }
        // }
        // if !success {
        //     warn!("could not parse user input");
        // }
    });
}
